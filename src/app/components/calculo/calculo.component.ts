import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {

  num1: number = 28;
  num2: number = 37;

  resul: number;



  constructor() {
    this.resul= this.num1 + this.num2;
  }

  ngOnInit(): void {
  }

 

}
